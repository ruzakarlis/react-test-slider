# React test - Slider #
Implemented by Karlis Edgars Ruza
karlis.ruza@scandiweb.com

# How to run? #
Clone this repo and run the following command inside the project directory:

npm install && npm run start

This should install all the dependencies, start the webpack development server and open your browser.

NB! The start script is configured to open google chrome. If receiving a error related to --open args,
make sure to edit the browser arg to a browser present on your machine. (after the --open in the start 
script under "scripts" in package.json)

# How to use? #
The following slider implementation supports multiple slides per slider page, infinite mode and it is
also resizable. To use the slider and for it to work, you need to pass the following props to the <Slider> component

* slidesPerPage - determines how many slides to show on each page of the slider

* sliderWidth - determines the width of the slider. As the slider is intended to be used and tested width viewport
width, the prop should be a value from 1 - 100vw passed as a string, e.g '80vw'

* infinite - determines whether infinite mode is enabled or disabled. This prop value should be a boolean value.

* buttons - determines whether next/previous buttons should be render, take note that this slider does not have
dragability implemented, so if this prop would be passed as false, the only means for controlling the slider would
ne touch controls.

* children - slide contents, not passed as regular props but passed as content inside the slider, e.g
    ...
    <Slider>
        <div>Slide 1</div>
        <div>Slide 2</div>
        ...
        <div>Slide n</div>
    </Slider>

note that no styles are applied to the content passed as children, except the width, which is set to sliderWidth/slidesPerPage.
specific styles for the content can be added for the children, if you wish to do so. 