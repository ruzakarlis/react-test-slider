import React from 'react';
import Slider from '../Slider/Slider'
import './App.scss'

export class App extends React.PureComponent {
    render() {
        const style = {background: 'blue'};

        return(
            <Slider
                buttons = { true }               
                sliderWidth = { '100vw' }
                buttons = { true } 
                infinite = { true }
                slidesPerPage = { 2 }
            >
                <div className="Slide Slide1">Slide 1</div>
                <div className="Slide Slide2">Slide 2</div>
                <div className="Slide Slide3">Slide 3</div>
                <div className="Slide Slide4">Slide 4</div>
                <div className="Slide Slide5">Slide 5</div>
                <div className="Slide Slide6">Slide 6</div>
            </Slider>
        );
    }
}

export default App;