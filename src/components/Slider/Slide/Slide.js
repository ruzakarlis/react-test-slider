import React from 'react';
import { PureComponent } from 'react';

import './Slide.scss';

class Slide extends PureComponent {
    render() {
        const { content, slideWidth } = this.props;
        return(       
            <div className="Slide" style = { slideWidth }>
                { content }
            </div>
        );
    }
}

export default Slide;