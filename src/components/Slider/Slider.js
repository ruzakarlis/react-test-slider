import React from 'react';
import regeneratorRuntime from "regenerator-runtime";
import { findDOMNode } from 'react-dom';    

import Slide from './Slide/Slide';
import SliderPagination from './SliderPagination/SliderPagination';
import './Slider.scss';

class Slider extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            slideCount: props.children.length,
            currentPage: 1,
            offset: 0,
            transition: 'left 0s linear'
        }
    }

    componentDidMount() {
        const { infinite, slidesPerPage } = this.props;
        if(infinite) {
            this.setState({
                offset: this.getSlideWidth() *  slidesPerPage
            })
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const { 
            currentPage,
            btnClicked
        } = this.state;
        const {
            slidesPerPage,
            slideCount
        } = this.props;
        
        if(currentPage !== 0 && currentPage <= (slideCount / slidesPerPage)) {
            const slider = document.querySelectorAll(".SliderWrapper-Slider")[0];
            slider.style.transition = 'left .3s linear';
        }
        else if(btnClicked && currentPage !== 0) {
            const slider = document.querySelectorAll(".SliderWrapper-Slider")[0];
            slider.style.transition = 'left .3s linear';
        }

    }

    renderSlides() {
        const { children, slidesPerPage, infinite } = this.props;
        const { currentPage, slideCount } = this.state;

        const slides =  children.map(this.renderSlide);
        //if infinite mode, add extra slides from the begining
        if(infinite) {
            for(let i = 0; i < slidesPerPage; i++) {
                slides.push(this.renderSlide(children[i]));
            }
            for(let i = slideCount - 1; i > slideCount - slidesPerPage - 1; i--) {
                slides.unshift  (this.renderSlide(children[i]));
            }
        }
        return slides;
    }

    renderSlide = (content) => {
        const slideWidth  = this.getSlideWidth() + 'px';
        const slideStyle = {
            width: slideWidth
        }
        return(<Slide content = { content } slideWidth = { slideStyle } />)
    }

    getSlideWidth() {
        const { slidesPerPage, sliderWidth } = this.props;
        const slideWidth = parseInt(sliderWidth) * (parseInt(document.documentElement.clientWidth) / 100) / slidesPerPage;

        return slideWidth;
    }

    handleTouchStart = (e) => {
        const { 
            offset,
            currentPage,
            slideCount
        } = this.state;
        const { slidesPerPage } = this.props;
        const { clientX } = e.targetTouches[0]; // Location of initial touch
    
        const slider = document.querySelectorAll(".SliderWrapper-Slider")[0];
        slider.style.transition = null;

        let newOffset, newCurrentPage;
        if(currentPage === 0) {
            newOffset = slideCount * this.getSlideWidth();
            newCurrentPage = slideCount / slidesPerPage;
        }
        else if(currentPage > (slideCount / slidesPerPage)) {
            newOffset = slidesPerPage * this.getSlideWidth();
            newCurrentPage = 1;
        }
        else {
            newOffset = offset;
            newCurrentPage = currentPage;
        }
        
        // add current offset to consider which page the user is on
        this.setState({
            offset: newOffset,
            transition: 'left 0s linear',
            touchStart: clientX + newOffset,
            currentPage: newCurrentPage,
            btnClicked: false
        });
    }

    handleTouchMove = (e) => {
        const { touchStart, offset } = this.state;
        const { clientX } = e.changedTouches[0];
        this.setState({
            offset: Math.round(touchStart - clientX)
        });
    }

    // Logic for deciding whether to go to next or previous slide
    handleTouchEnd = (e) => {
        const { 
            touchStart, 
            currentPage, 
            slideCount, 
            offset 
        } = this.state;
        const {
            infinite,
            slidesPerPage
        } = this.props
        const { clientX } = e.changedTouches[0];
        const slideWidth = this.getSlideWidth();

        const newState = {};
        // Finite mode
        if(!infinite) {
            // Swipe right (next slide) 
            if((touchStart - (currentPage - 1) * slideWidth - clientX) > slideWidth/2) {
                // When trying to swipe after last slide
                if(currentPage * slideWidth > slideWidth * (slideCount - 1)) {
                    newState.transition = 'left .1s linear';
                    newState.offset = slideWidth * (currentPage - 1);
                }
                // Swipe to next slide
                else {
                    newState.transition = 'left .1s linear';
                    newState.offset = currentPage * slideWidth;
                    newState.currentPage = currentPage + 1;
                }
            }
            // Swipe left (prev slide)
            else{
                // When trying to swipe befoe first slide
                if(currentPage - 1 === 0) {
                    newState.transition = 'left .1s linear';
                    newState.offset = 0;
                    newState.currentPage = 1;
                }
                //Swipe to previous slide
                else {
                    newState.transition = 'left .1s linear';
                    newState.offset = (currentPage - 2) * (slideWidth);
                    newState.currentPage = currentPage - 1;
                }
            }
        }
        // Infinite mode, swipe next
        else if((touchStart - ((currentPage - 1) * slidesPerPage) * slideWidth - clientX - slidesPerPage * slideWidth) > slideWidth/2) {
            // When trying to swipe after last slide;
            if(currentPage * slideWidth > slideWidth * (slideCount - 1)) {
                if(currentPage > slideCount / slidesPerPage) {
                    newState.offset = (1 + slidesPerPage) * slideWidth;
                    newState.currentPage = 2;
                }
                else {
                    newState.transition = 'left .1s linear';
                    newState.offset = (currentPage + slidesPerPage) * slideWidth;
                    newState.currentPage = currentPage + 1;
                }
            }
            // Swipe to next slide
            else {
                newState.transition = 'left .1s linear';
                newState.offset = (currentPage * slidesPerPage + slidesPerPage) * slideWidth;
                newState.currentPage = currentPage + 1;
            }
        }
        // Swipe left (prev slide)
        else {
            //Swipe to previous slide     
            if(Math.abs((touchStart - ((currentPage - 1) * slidesPerPage) * slideWidth) - clientX - slideWidth * slidesPerPage) > slideWidth/2) {
                // When going to last slide from first
                if(currentPage === 0) {
                    newState.transition = 'left .1s linear';
                    newState.offset = (slideCount - 2 + slidesPerPage) * slideWidth;
                    newState.currentPage = slideCount / slidesPerPage - 1;
                }
                else {
                    newState.transition = 'left .1s linear';
                    newState.offset = ((currentPage - 2) * slidesPerPage  + slidesPerPage) * slideWidth;
                    newState.currentPage = currentPage - 1;
                }
            }
            else {
                newState.transition = 'left .1s linear';
                newState.offset = (currentPage) * slidesPerPage * slideWidth;
                newState.currentPage = currentPage;
            }
        }
        this.setState(newState);
    }

    renderButtonBack() {
        const { buttons } = this.props;
        if(buttons) {
            return(
                <button 
                    onClick = { this.prevPage }
                    className = 'SliderWrapper-BtnBack'
                >
                    Back
                </button>
            );
        }
    }

    renderButtonNext() {
        const { buttons } = this.props;
        if(buttons) {
            return(
                <button 
                    onClick = { this.nextPage }
                    className = 'SliderWrapper-BtnNext'
                >
                    Next
                </button>
            );
        }
    }
    
    // Handle next button click
    nextPage = () => {
        const { 
            infinite,
            slidesPerPage
        } = this.props;
        const { 
            currentPage, 
            slideCount, 
            offset 
        } = this.state;
        const slideWidth = this.getSlideWidth();

        if(currentPage === (slideCount / slidesPerPage) && !infinite) {
            return;
        }
        /**
         * The currentPage is greater than the possible amount of pages, due to the infinite setting
         * which means the slider is showing the first page but at the end of the array
        **/
        else if(currentPage > slideCount/slidesPerPage && infinite) {
            const newOffset = -slidesPerPage * this.getSlideWidth()
            const slider = document.querySelectorAll(".SliderWrapper-Slider")[0];
            slider.style.transition = 'left 0s linear';
            slider.style.left = newOffset.toString() + 'px';

            this.setState({     
                currentPage: 2,
                transition: 'left .3s linear',  
                offset: this.getSlideWidth() * 2 * slidesPerPage,
                btnClicked: true
            });
        }
        else {
            this.setState({ 
                transition: 'left .3s linear',
                currentPage: currentPage + 1,
                offset: offset + slideWidth * slidesPerPage,
                btnClicked: true
            });
        }
    }

    // Handle prev button click
    prevPage = () => {
        const { infinite, slidesPerPage } = this.props;
        const { currentPage, slideCount, offset } = this.state;
        const slideWidth = this.getSlideWidth();

        if(currentPage === 1 && !infinite) {
            return;
        }
        else if(infinite && currentPage === 0) {
            const newOffset = -(slideWidth * slideCount);
            const slider = document.querySelectorAll(".SliderWrapper-Slider")[0];
            slider.style.transition = 'left 0s linear';
            slider.style.left = newOffset.toString() + 'px';

            this.setState({ 
                currentPage: slideCount / slidesPerPage - 1, 
                offset: slideWidth * (slideCount - slidesPerPage),
                btnClicked: true
            });
        }
        else if(infinite && currentPage === 1) {
            this.setState({ 
                transition: 'left .3s linear',  
                currentPage: 0, 
                offset: offset - slideWidth * slidesPerPage,
                btnClicked: true
            });
        }
        else {
            this.setState({ 
                currentPage: currentPage - 1,
                offset: offset - slideWidth * slidesPerPage,
                btnClicked: true
            });
        }
    }

    paginationCallback = (page) => {
        const { slidesPerPage, infinite } = this.props;
        const slideWidth = this.getSlideWidth();
        if(infinite) {
            this.setState({ 
                currentPage: page + 1,
                offset: slidesPerPage * slideWidth + page * slidesPerPage * slideWidth,
                btnClicked: true
            });
        }
        else {
            this.setState({ 
                currentPage: page + 1,
                offset: page * slidesPerPage * slideWidth,
                btnClicked: true
            });
        }
    }

    render() {
        const { 
            offset, 
            transition, 
            slideCount, 
            currentPage 
        } = this.state;

        const { 
            sliderWidth,
            infinite,
            slidesPerPage
        } = this.props;

        const sliderStyle = {
            transition: transition,
            left: -offset
        };
        const sliderWrapperWidth = { width: sliderWidth };

        return ( 
            <div className = 'SliderWrapper' style = { sliderWrapperWidth }>
                { this.renderButtonBack() }
                <div 
                    className="SliderWrapper-Slider" 
                    style = { sliderStyle }
                    onTouchStart={ this.handleTouchStart } 
                    onTouchMove={ this.handleTouchMove } 
                    onTouchEnd={ this.handleTouchEnd }
                >
                    { this.renderSlides() }
                </div>
                { this.renderButtonNext() }
                <SliderPagination 
                    paginationCallback = { this.paginationCallback }
                    slideCount = { slideCount }
                    currentPage = { currentPage }
                    infinite = { infinite }
                    slidesPerPage = { slidesPerPage }
                />
            </div>
        );
    }
}

export default Slider;