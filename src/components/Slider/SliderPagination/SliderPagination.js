import React from 'react';

import './SliderPagination.scss';

class SliderPagination extends React.PureComponent {
    
    renderPagination() {
        const { 
            slideCount, 
            paginationCallback, 
            currentPage, 
            infinite,
            slidesPerPage 
        } = this.props;
        const dots = [];

        for(let i = 0; i < slideCount / slidesPerPage; i++) {
            if(currentPage > slideCount/slidesPerPage && i === 0 && infinite) {
                dots.push(<div className='SliderWrapper-Dots-isActive' onClick={() => paginationCallback(i) } />);
            }
            else if(currentPage === 0 && infinite && i === slideCount / slidesPerPage - 1){
                dots.push(<div className='SliderWrapper-Dots-isActive' onClick={() => paginationCallback(i) } />);
            }
            else if(i === currentPage - 1) {
                dots.push(<div className='SliderWrapper-Dots-isActive' onClick={() => paginationCallback(i) } />);
            }
            else { 
                dots.push(<div className='SliderWrapper-Dots' onClick={() => paginationCallback(i) } />);
            }
        }

        return dots;
    }

    render() {
        return (
            <div className="SliderWrapper-DotContainer">
                { this.renderPagination() }
            </div>
        );
    }
}

export default SliderPagination;