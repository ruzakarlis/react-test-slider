const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    entry: './src/index.js',
    devtool: 'inline-source-map',
    performance: { 
        hints: false 
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template:"./src/index.html",
            filename:"./index.html"
        }),
    ],
    module: {
        rules:[
            {
                test:/\.(js|jsx)$/,
                exclude:/node_modules/,
                use: {
                    loader:'babel-loader',
                    options: { 
                        presets: ['@babel/preset-env', '@babel/react'],
                        plugins:['@babel/plugin-proposal-class-properties']
                    }
                },
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                  'style-loader',
                  'css-loader',
                  'sass-loader',
                ]
            }
        ],
    },
    devServer: {
        contentBase: './dist',
        port: '9000',
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
    }
};